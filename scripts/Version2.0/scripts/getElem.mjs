const root = document.getElementById("root");
const popup = document.getElementById("popup");
const textInput = document.getElementById("text-input");
const form = document.getElementById("form");
const next = document.getElementById('next');
const name = document.getElementById('divName');

export {root, popup, textInput, form, next, name};